#!/usr/bin/env bash

recursive_ppid_string_f(){
	if [ -z "$2" ]; then
		echo "First argument (string to search in parents) needed"
		echo "Second argument (initial pid) needed"
		echo "[Third argument] (number of times to ignore) is optional"
		return 1
	fi

	local PS_LIST="$(ps -eo 'pid,ppid,args')"
	local res="$(awk ''$2'==$1{print $0}' <<< "$PS_LIST")"
	local last
	local temp
	local i
	local n
	local count=0
	if [ "$(awk -v a=$3 'BEGIN {print (a == a + 0)}')" -eq 1 ]; then 		#if is a number
		count=$3
	fi
	while [ -n "$res" ]; do
		last="$res"
		res=""
		while read -r i; do
			if [ -n "$(grep "$1" <<< "$i")" ]; then
				count=$((count-1))
				if [ "$count" -lt 0 ]; then
					echo "$i"
					return 0
				fi
			fi

			n="$(awk '{print $2}'<<<$i)" 			#recursion limit
			if [ 2 -gt "$n" ]; then
				res=""
				break
			fi

			temp="$(awk ''$n'==$1{print $0}' <<< "$PS_LIST")"
			if [ -n "$temp" ]; then
				if [ -z "$res" ]; then
					res="$temp"
				else
					res="$res
$temp"
				fi
			fi
		done <<< "$last"
	done
	return 1
}

if [ -z "$2" ]; then
	recursive_ppid_string_f "$1" "$$" "1"
else
	recursive_ppid_string_f "$@"
fi
