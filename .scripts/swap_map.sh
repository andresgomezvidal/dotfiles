#!/usr/bin/env zsh

if [[ "$(setxkbmap -query |awk 'END{print $2}')" == "us" ]]; then
	setxkbmap es;
else
	setxkbmap us;
fi

#xmodmap has a cpu hicup for some systems
if [[ "$xmodmap_lock" != true && -s ~/.Xmodmap ]]; then
	xmodmap ~/.Xmodmap
fi
