#!/usr/bin/env zsh

#$1 origin      (from where to copy/link)
#$2 destination (where to copy/link)
#this is for when you don't want to copy everything or want to share easily among several users


if [ -z "$sourced_functions_file" ]; then
	if [ -z "$shellc_path" ]; then shellc_path=. ; fi

	if [ -s "$shellc_path/functions_string.sh" ]; then
		. "$shellc_path/functions_string.sh"
	else
		wget "https://gitlab.com/andresgomezvidal/zsh_conf/raw/master/custom/functions_string.sh"
		. "$shellc_path/functions_string.sh" || exit 1
	fi

	if [ -s "$shellc_path/functions_file.sh" ]; then
		. "$shellc_path/functions_file.sh"
	else
		wget "https://gitlab.com/andresgomezvidal/zsh_conf/raw/master/custom/functions_file.sh"
		. "$shellc_path/functions_file.sh" || exit 1
	fi

	if [ -s "$shellc_path/functions_min.sh" ]; then
		. "$shellc_path/functions_min.sh"
	else
		wget "https://gitlab.com/andresgomezvidal/zsh_conf/raw/master/custom/functions_min.sh"
		. "$shellc_path/functions_min.sh" || exit 2
	fi
fi


if [ -n "$1" ]; then
	ori="$1"
	echo "Origin set to '$dest'"
else
	ori=~/dotfiles
fi

echo "Origing path: where to download dotfiles. Eg: /home/<username>/dotfiles"
echo "Destination path: where to cp/link downloaded dotfiles. Eg: /home"
echo ""
echo -n "Use '$ori' as origin path: [Y/n]"
read REPLY
echo ""		# (optional) move to a new line
if [[ $REPLY =~ ^[Nn]$ ]]; then
	echo -n "Please insert the path to use: "
	read REPLY
	echo ""		# (optional) move to a new line
	ori="$REPLY"
fi
if [ "$ori" = "$HOME" ]; then
	ori="$HOME"/dotfiles
	echo -e "Changed origing path from '$HOME' to '$ori'\n\n"
else
	echo -e "Using '$ori' as origin path\n\n"
fi


if [ -n "$2" ]; then
	dest="$2"
	echo "Destination set to '$dest'"
elif [ -d /data ]; then
	dest=/data
else
	dest=~
fi
echo -n "Use '$dest' as destination path: [Y/n]"
read REPLY
echo ""		# (optional) move to a new line
if [[ $REPLY =~ ^[Nn]$ ]]; then
	echo -n "Please insert the path to use: "
	read REPLY
	echo ""		# (optional) move to a new line
	dest="$REPLY"
fi
echo -e "Using '$dest' as destination path\n\n"


if ! [ -d "$ori/.git" ]; then
	if [ -e "$ori" ]; then
		echo -n "REMOVE '$ori' and download repo there?: [Y/n]"
		read REPLY
		echo ""		# (optional) move to a new line
		if ! [[ $REPLY =~ ^[Nn]$ ]]; then
			rm -rf "$ori"
		else
			echo "Could not continue because '$ori' already exists"
			exit 3
		fi
	fi
	echo "Downloading repo to '$ori'"
	gitlab_clone_donwload_f andresgomezvidal/dotfiles "$ori"
fi
cd "$ori"


ln_cp_bak_question_f

if [ -n "$selected_function_ln_cp_bak" ]; then
	mkdir -p "$ori"/.vim/bundle
	mkdir -p "$dest"/.local/share
	mkdir -p "$dest"/.config/nvim

	$selected_function_ln_cp_bak "$ori"/.scripts "$dest"/.scripts
	$selected_function_ln_cp_bak "$ori"/lesskeys "$dest"/lesskeys
	$selected_function_ln_cp_bak "$ori"/Templates "$dest"/Templates
	$selected_function_ln_cp_bak "$ori"/.latexmkrc "$dest"/.latexmkrc
	$selected_function_ln_cp_bak "$ori"/.licenses "$dest"/.licenses
	$selected_function_ln_cp_bak "$ori"/.qjoypad3 "$dest"/.qjoypad3
	$selected_function_ln_cp_bak "$ori"/.selected_editor "$dest"/.selected_editor
	$selected_function_ln_cp_bak "$ori"/.xinitrc "$dest"/.xinitrc
	$selected_function_ln_cp_bak "$ori"/.Xmodmap "$dest"/.Xmodmap

	$selected_function_ln_cp_bak "$ori"/.fonts "$dest"/.local/share/.fonts

	mkdir -p "$dest"/.config/qutebrowser/{default,private}/config
	$selected_function_ln_cp_bak "$ori"/.config/qutebrowser/config-default.py "$dest"/.config/qutebrowser/default/config/config.py
	$selected_function_ln_cp_bak "$ori"/.config/qutebrowser/config-private.py "$dest"/.config/qutebrowser/private/config/config.py

	$selected_function_ln_cp_bak "$ori"/.config/openbox "$dest"/.config/openbox
	$selected_function_ln_cp_bak "$ori"/.config/tint2 "$dest"/.config/tint2
	$selected_function_ln_cp_bak "$ori"/.config/redshift.conf "$dest"/.config/redshift.conf
	$selected_function_ln_cp_bak "$ori"/.config/terminator "$dest"/.config/terminator

	#often system overwritten files
	mkdir -p "$dest"/.config/caja
	mkdir -p "$dest"/.config/smplayer
	mkdir -p "$dest"/.config/vlc
	cp -vi "$ori"/.config/caja/accels "$dest"/.config/caja/accels
	cp -vi "$ori"/.config/smplayer/smplayer.ini "$dest"/.config/smplayer/smplayer.ini
	cp -vi "$ori"/.config/vlc/vlcrc "$dest"/.config/vlc/vlcrc


	if reply_q_f "Install a conky file?" "false"; then
		echo "Choose among:"
		echo -e "\tLaptop with nvidia:  \t 1"
		echo -e "\tDesktop with nvidia: \t 2"
		echo -e "\tDesktop with radeon: \t 3"
		read REPLY
		echo ""		# (optional) move to a new line
		if [[ $REPLY =~ 1$ ]]; then
			$selected_function_ln_cp_bak "$ori"/.conkyrc_laptop_nvidia "$dest"/.conkyrc
		elif [[ $REPLY =~ 2$ ]]; then
			$selected_function_ln_cp_bak "$ori"/.conkyrc_desktop_nvidia "$dest"/.conkyrc
		elif [[ $REPLY =~ 3$ ]]; then
			$selected_function_ln_cp_bak "$ori"/.conkyrc_desktop_radeon "$dest"/.conkyrc
		fi
	fi

	$selected_function_ln_cp_bak "$ori"/.vimrc "$dest"/.vimrc
	$selected_function_ln_cp_bak "$ori"/.vimrc_common "$dest"/.vimrc_common
	cp "$ori"/.vimrc_plugins "$dest"/.vimrc_plugins
	$selected_function_ln_cp_bak "$ori"/.ideavimrc "$dest"/.ideavimrc
	$selected_function_ln_cp_bak "$ori"/.vimrc "$dest"/.nvimrc
	$selected_function_ln_cp_bak "$ori"/.vimrc "$dest"/.config/nvim/init.vim

	if ! [ -d ~/.vim/bundle/Vundle.vim ]; then
		echo "Installing vundle for vim"
		git_clone_download_f https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
		echo -n "Do you want to download all vim's plugins now?: [Y/n]"
		read REPLY
		echo ""		# (optional) move to a new line
		if ! [[ $REPLY =~ ^[Nn]$ ]]; then
			vim +PluginUpdate +qall
		fi
	fi
else
	echo "No action selected"
fi
