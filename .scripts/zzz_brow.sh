#!/usr/bin/env zsh

#script example
#visit pages every x PERIOD (number argument)

if [ -z "$shellc_path" ]; then shellc_path=~/Shell/custom/ ; fi

. "$shellc_path/functions_min.sh"
. "$shellc_path/functions_download.sh"


#for ignoring the time to wait, but asking first for confirmation
ARG1="$1"

#checksum files path
u="$HOME"/.zzz_checksums
mkdir -p "$u"

#simplicity and just moving around lines
simple_do_check_update_date_f(){
	if [ -n "$ARG1" ] && reply_q_f "Visit $2?" "false"; then
		do_check_update_date_f "$1" "$2" "$PERIOD" "$u" 'second'
	else
		do_check_update_date_f "$1" "$2" "$PERIOD" "$u" "$3"
	fi
}



do_check_update_f "firefox --private-window" "http://www." 2 "$u" 'hour' &

do_check_update_f "firefox --private-window" "http://www." 20 "$u" 'day' &




BROWSER="firefox"
PERIOD=1
T=day
simple_do_check_update_date_f "chromium --incognito" "https://www..com"
simple_do_check_update_date_f "$BROWSER" "https://www.reddit.com/r//top/?sort=top&t=$T"



PERIOD=7
T=week
simple_do_check_update_date_f "$BROWSER" "https://www.reddit.com/r//top/?sort=top&t=$T"

PERIOD=30
T=month
simple_do_check_update_date_f "$BROWSER" "https://www.reddit.com/r/commandline/top/?sort=top&t=$T"

