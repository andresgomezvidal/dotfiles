#!/usr/bin/env bash


if [ -n "$1" ]
then
        url="$1"
else
        url="$(xsel -bo)"
fi
#url=$( echo -n "${1%%'&'*}" ) #remove & and after

#nohup \mpv --load-unsafe-playlists "$url" "${@:2:$#}" &> /dev/null &
nohup \mpv "$1" "${@:2:$#}" &> /dev/null &
