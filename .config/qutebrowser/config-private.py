config.load_autoconfig()
config.bind('J', 'scroll-page 0 0.5')
config.bind('K', 'scroll-page 0 -0.5')
config.bind('<Alt-l>', 'tab-next')
config.bind('<Alt-h>', 'tab-prev')
config.bind('b', 'back')
config.bind('B', 'forward')

config.bind('m', 'spawn bash -c "nohup mpv {url} --load-unsafe-playlists &> /dev/null &"')
# config.bind('M', 'hint links spawn bash -c "nohup mpv {hint-url} --cache=200000 --load-unsafe-playlists &> /dev/null &"')
# config.bind('M', 'hint links spawn bash -c "nohup mpv {hint-url} --load-unsafe-playlists &> /dev/null &"') #it crashes with --cache
config.bind('M', 'hint links spawn bash -c "~/.scripts/mpv_link.sh {hint-url}"')

c.url.searchengines = {
   "DEFAULT":'https://duckduckgo.com/?q={}',
   "g":'https://www.google.com/search?q={}',
   "y": 'https://www.youtube.com/results?search_query={}',
   "wa": "https://wiki.archlinux.org/?search={}",
}

config.set('tabs.background',True)
config.set('downloads.location.directory', '~/Downloads')
config.set('zoom.default', '110%')
config.set('url.default_page', 'about:blank')
config.set('url.start_pages', 'about:blank')


#DEFAULT/PRIVATE CHANGES
config.set('content.private_browsing',True)

#with default (grey or #666) I cannot see the pointer
config.set('colors.statusbar.command.private.bg','#333')
config.set('colors.statusbar.private.bg','#333')
