if [ -e /tmp/my-feh-background.lock ]; then
	DISPLAY=:0.0 feh --bg-max ~/.config/.wallpapers_conky/black.png &
else
	DISPLAY=:0.0 feh --bg-max "$(find ~/.config/.wallpapers_conky/ -type f |sort -R |tail -1)" &
fi
