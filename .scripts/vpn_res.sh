#!/usr/bin/env bash


LOCK=/tmp/vpn_res_block.lock
FOVPN=/etc/openvpn/fav/fav.ovpn
DLOG_PERSONAL=/var/ulog
LOG="$DLOG_PERSONAL"/ivpn.log
ELOG="$DLOG_PERSONAL"/ivpn_error.log
waito=10
waitt=25
while=15
waitom=$((waito*1000))
waittm=$((waitt*1000))
whilem=$((while*800))
waitm=3000

route_string='tun0'


OLDPWD="$PWD"
cd ~

exec 2>> "$ELOG"

export DISPLAY=${DISPLAY:=":0"}
export XAUTHORITY="${XAUTHORITY:=$HOME/.Xauthority}"
export DBUS_SESSION_BUS_ADDRESS=${DBUS_SESSION_BUS_ADDRESS:="unix:path=/run/user/$UID/bus"}


pid_lock_f(){
	PID_LOCK=/tmp/vpn_res_pid.lock
	#if locks exists and contains a valid pid exit, else write its pid and continue
	#so only one process is in at a time
	if [[ -s "$PID_LOCK" && -n "$(awk '$1=='$(cat $PID_LOCK)' {print $0}' <<< "$(ps -eo pid)")" ]]; then
		exit 1
	else
		echo "$$" > "$PID_LOCK"
	fi
}

check_lock_f(){							#if used with cron or crashes I want a way to kill it easily
	if [ -e "$LOCK" ]; then
		exit 2
	fi ;
}

check_isCorrect_f(){
	#gather info on download and upload bytes on any interface, sum them, to know if the total is greather than 0
	local f1=`cat /proc/net/dev`
	sleep 4 		#window time margin to gather info on download and upload bytes
	local f2=`cat /proc/net/dev`
	local total=0
	while read -r i; do
		r=`awk '/'$i'/ {i++; rx[i]=$2; tx[i]=$10}; END{print rx[2]-rx[1] " " tx[2]-tx[1]}' <<< "$f1" <<< "$f2"`
		r1=`cut -d ' ' -f1 <<< "$r"`
		r2=`cut -d ' ' -f2 <<< "$r"`
		((total+=r1 + r2))
	done <<< `ip addr |awk -F':' '$1~/^[0-9]/ {print $2}'`
	#if there is something downloading or uploading, then it is fine
	if ! [ "$total" -eq 0 ]; then
		return 0
	fi


	local startTime=`date '+%Y%m%d%H%M%S'`;
	local routeResult="$(route)";
	local diffTime=$(( `date '+%Y%m%d%H%M%S'` - $startTime ));
	if [ -z "$1" ]; then 					#para no comprobarlo si no está activo el vpn, y es + fácil-eficiente que ps|grep
		if [ -z "$(grep "$route_string" <<< "$routeResult")" ]; then
			echo "Route no contiene $route_string" >> "$LOG"
			return 1
		fi
	fi
	if [ "$diffTime" -gt 10 ]; then
		echo "Demasiado tiempo ($diffTime) de route" >> "$LOG"
		return 1
	fi
	if [ $(wc -l <<< "$routeResult") -le 2 ]; then
		echo "Lineas insuficientes de route" >> "$LOG"
		echo "$routeResult" >> "$LOG"
		return 1
	fi

	if [ $(wget www.google.com -O /dev/null &> /dev/null; echo "$?") -ne 0 ]; then
		echo "Problema wget" >> "$LOG"
		return 1
	fi

	#echo "Final de función check_isCorrect_f" >> "$LOG"
	return 0;
}



pid_lock_f


if [ -n "$(ps -aux|grep /sbin/openvpn|grep -v grep)" ]; then
	if ! check_isCorrect_f; then
		echo "Problema detectado en check_isCorrect_f con vpn activo" >> "$LOG"
		echo "Resetting at $(date +%Y/%m/%d-%H:%M:%S)" >> "$LOG"
		/usr/bin/notify-send -t $waitom 'VPN caido' "Matando VPN" &
		sudo killall openvpn; sleep $waito;
	fi

	while ! check_isCorrect_f; do
		check_lock_f
		echo "Problema detectado en bucle check_isCorrect_f" >> "$LOG"
		echo "Resetting at $(date +%Y/%m/%d-%H:%M:%S)" >> "$LOG"
		sudo killall openvpn; sleep $waito; sudo /sbin/openvpn --config "$FOVPN" &
		/usr/bin/notify-send -t $whilem 'reiniciando... VPN' "Esperando $while segundos" &
		sleep $while
	done

else
	check_lock_f
	#if there is no internet connection, then don't log anything
	if [ "$(route|wc -l)" -le 2 ]; then 						#this should be fast so there is no need to use a variable
		exit 3
	fi

	echo "Not running. $(date +%Y/%m/%d) $(uptime |cut -d ',' -f1)" >> "$LOG"

	while ! check_isCorrect_f "simple"; do
		check_lock_f
		echo "Problema detectado en check_isCorrect_f sin vpn activo $(date +%Y/%m/%d-%H:%M:%S)" >> "$LOG"
		sleep $while
	done

	/usr/bin/notify-send -t $waitom 'Iniciado VPN' &
	sudo /sbin/openvpn --config "$FOVPN" &
fi

cd "$OLDPWD"
