#!/usr/bin/env bash

tries_max=300
while true; do
	tries=0
    vlow=0
    vtotal=0 #no need to worry about overflow because it is never going to come close
    prev_message=0
	while [ "$tries" -lt "$tries_max" ]; do
        vtemp="$(ifstat | awk '$0~/tun0/ {print $2}')"
        if [ ! $(awk -v a="$vtemp" 'BEGIN {print (a == a + 0)}') -eq 1 ]; then #convert to 0 if the command didn't result in a number
            echo "vtemp=$vtemp was not a number"
            vtemp=0
        fi
        vtotal=$((vtotal+vtemp))
		tries=$((tries+1))
        if [ "$vtemp" -lt 60 ]; then #count the amount of times traffic is low
            vlow=$((vlow+1))
            if [ "$prev_message" -eq 0 ]; then
                prev_message=1
                echo -e "\n$(date)"
            fi
            echo "tries=$tries/$tries_max vlow=$vlow vtemp=$vtemp vtotal=$vtotal"
        fi
		sleep 1
	done

    if [ -z "$(ifconfig | grep tun0)" ]; then echo 'vpn not running'; continue; fi #check if the vpn is running

    if [[ "$vlow" -gt "$(( 3 * tries_max/4))" && "$vtotal" -lt "$((tries_max * 30 ))" ]]; then
        echo "vlow=$vlow/$tries_max vtotal=$vtotal"
        echo 'qbittorrent got stuck, restarting...'
        break #it only gets stuck once, after restarting it once after getting stuck it works perfectly well
    fi
done


if [ -z "$(ifconfig | grep tun0)" ]; then echo 'vpn not running'; exit 1; fi #check if the vpn is running

qbittorrent_pid="$(ps aux|grep -v grep |grep -v $$ | awk '$11=="qbittorrent" {print $2}')"
if [ -n "$qbittorrent_pid" ]; then
    kill "$qbittorrent_pid"
    sleep 10
    kill -9 "$qbittorrent_pid"
    sleep 10
else
    echo "qbittorrent is not running"
fi
\nohup qbittorrent &> /dev/null &


