#!/usr/bin/env zsh

startup_lock=/tmp/startup.lock
if [[ -e "$startup_lock" ]]; then
	exit 1
fi

connection_check=false

wait_route_f(){
	local tries=0
	local route_string='default'
	while [ "$tries" -lt 20 ]; do
		sleep 5
		local route_grep="$(route |grep $route_string |grep -v grep|grep -v 'docker'|grep -v 'br')"
		if [[ -n "$route_grep" ]]; then
            connection_check=true
            return 0
			break
		fi
		tries=$((tries+1))
	done
    return 1
}

wait_vpn_f(){
	local tries=0
	local route_string='tun0'
	while [ "$tries" -lt 10 ]; do
		sleep 5
		local route_grep="$(route |grep $route_string |grep -v grep)"
		if [[ -n "$(ps -aux|grep /sbin/openvpn|grep -v grep)" && -n "$route_grep" ]]; then
			break
		fi
		tries=$((tries+1))
	done
}

xdotool_max_terminal_f(){
    if [[ -n "$startup_terminal_lock" && "$startup_terminal_lock" == true ]]; then
        return 0
    fi
	terminal_manager
    if [ "$(xrandr |grep ' connected'|wc -l)" -eq 1 ]; then
        return 0
    fi
	sleep 2
	wmctrl -r "$terminal_manager"     #selecciona ventana terminal
	xdotool key ctrl+Shift_L+Right; xdotool key ctrl+Shift_L+Right; sleep .1; xdotool key ctrl+Shift_L+Right; xdotool key ctrl+Shift_L+Right; sleep .1; xdotool key ctrl+Shift_L+Right; xdotool key ctrl+Shift_L+Right; sleep .1
	sleep .1
	xdotool key alt+F5
	sleep .2
	xdotool key alt+F11
}







\conky &
#\redshift &
\unclutter -idle 30 &
#\autokey-gtk &
#remmina -i &
music_manager
#~/.scripts/feh.sh &


wmctrl -s 0
if [ "$startup_terminal_lock" != true ]; then
	terminal_manager
	sleep 1
	if [ "$tmuxinator_lock" != true ]; then
		xdotool type "tmuxinator start start"
		sleep .2
		xdotool key KP_Enter
	fi
fi

wait_route_f && {
wmctrl -s 1
email_manager
sleep 2
#\pidgin &
#sleep 10
}

#wmctrl -s 3
#sleep 2
#keepassx &
#sleep 1
#xdotool_max_terminal_f
#sleep 1

wmctrl -s 0
ifnps conky

DIR="${0%'/'*}"/

if [ "$HOST" = "torre" ]; then
	/usr/lib/gsd-clipboard & 																	#gnome-screnshot clipboard option
	xrandr --output $(xrandr 2> /dev/null| awk '$2=="connected" {print $1}') --primary &		#sets the connected output as primary
	if [ "$(cat /proc/uptime|cut -d . -f1)" -lt "600" ]; then
		file_manager /mybackup/
		file_manager /
		sleep 4
		killall_r $file_manager
	fi

    if [ "$connection_check" = true ]; then
        if [ "$vpn_lock" != true ]; then
            nohup "$DIR/vpn_res.sh"
            wait_vpn_f
        else
            touch /tmp/vpn_res_block.lock
        fi

        if [ "$telegram_lock" != true ]; then
            Telegram
        fi
    fi

	if [[ "$(cat /proc/uptime|cut -d . -f1)" -lt "600" ]]; then
		sudo hdparm -S 36 /dev/sdd
		sleep 60
		sudo hdparm -y /dev/sdd
	fi
fi
