#!/usr/bin/env bash

#wrapper for taking screenshots with different options and available programs

command_exists_hash_f () {
	hash "$1" 2>/dev/null || command -v "$1" &>/dev/null ;
}
fall_back_hash_f () {
	local i
	for i in "$@"
	do
		if command_exists_hash_f $(cut -d ' ' -f1 <<< $i) ; then
			echo $i
			break;
		fi
	done;
}

program=$(fall_back_hash_f scrot escrotum gnome-screenshot xfce4-screenshooter)
#maybe later ##flameshot maim

if [ -n "$program" ]; then
	name=~/Pictures/Screenshot_$(date '+%Y-%m-%d-%H.%M.%S').png
	temp=/tmp/temp_image_clipboard.png

	if [ -z "$1" ]; then 			#no options, fullscreen
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -f "$name"
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -f -s "$name"
		elif [ "$program" = 'scrot' ]; then
			scrot "$name" -z -p -q 100
		elif [ "$program" = 'escrotum' ]; then
			escrotum "$name"
		fi
	elif [ "$1" = c ]; then 		#copy, fullscreen
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -c
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -f -c
		elif [ "$program" = 'scrot' ]; then
			scrot "$temp" -o -z -p -e 'xclip -selection clipboard -target image/png -i $f'
		elif [ "$program" = 'escrotum' ]; then
			escrotum -C
		fi
	elif [ "$1" = s ]; then 		#selection
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -a -f "$name"
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -r -s "$name"
		elif [ "$program" = 'scrot' ]; then
			scrot "$name" -z -p -s
		elif [ "$program" = 'escrotum' ]; then
			escrotum "$name" -s
		fi
	elif [ "$1" = cs ]; then 		#copy selection
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -a -c
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -r -c
		elif [ "$program" = 'scrot' ]; then
			scrot "$temp" -o -z -p -s -e 'xclip -selection clipboard -target image/png -i $f'
		elif [ "$program" = 'escrotum' ]; then
			escrotum -s -C
		fi
	elif [ "$1" = a ]; then 		#focused window
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -w -f "$name"
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -w -s "$name"
		elif [ "$program" = 'scrot' ]; then
			scrot "$name" -z -p -u
		elif [ "$program" = 'escrotum' ]; then
			escrotum "$name" -x
		fi
	elif [ "$1" = ca ]; then 		#copy focused window
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -w -c
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -w -c
		elif [ "$program" = 'scrot' ]; then
			scrot "$temp" -o -z -p -u -e 'xclip -selection clipboard -target image/png -i $f'
		elif [ "$program" = 'escrotum' ]; then
			escrotum -x -C
		fi
	elif [ "$1" = d ]; then 		#delay, fullscreen
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -d 5 -f "$name"
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -f -d 5 -s "$name"
		elif [ "$program" = 'scrot' ]; then
			scrot "$name" -z -p -d 5
		elif [ "$program" = 'escrotum' ]; then
			escrotum "$name" -d=5
		fi
	elif [ "$1" = cd ]; then 		#copy delay, fullscreen
		if [ "$program" = 'gnome-screenshot' ]; then
			gnome-screenshot -p -d 5 -c
		elif [ "$program" = 'xfce4-screenshooter' ]; then
			xfce4-screenshooter -m -f -d 5 -c
		elif [ "$program" = 'scrot' ]; then
			scrot "$temp" -o -z -p -d 5 -e 'xclip -selection clipboard -target image/png -i $f'
		elif [ "$program" = 'escrotum' ]; then
			escrotum -d=5 -C
		fi
	fi
fi
