#!/usr/bin/env zsh

#lock here to launch the script directly with iamback_f
if [[ "$email_lock" != true && -z "$(ps -e|grep $email_manager|grep -v $email_manager)" ]]; then
	email_manager
	sleep 3

	#wmctrl -a "$email_manager" &&
	#xdotool key --clearmodifiers "alt+F3"
	#wmctrl -r "$email_manager" -b toggle,shaded

    if [ "$(wmctrl -d |wc -l)" -gt 0 ]; then
		current=$(wmctrl -d | gawk '{if ($2 == "*") print $1}')
	    #dest=$(wmctrl -d |awk 'END{print $1}')
        dest=$((current + 1))
		wmctrl -r "$email_manager" -t "$dest"
	fi
fi
